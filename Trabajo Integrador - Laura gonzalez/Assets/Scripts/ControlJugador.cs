using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJugador : MonoBehaviour
{
    private Rigidbody rb;
    public int rapidez;
    public TMPro.TMP_Text CantidadGBRecolectados;
    public TMPro.TMP_Text CantidadGRRecolectados;
    public TMPro.TMP_Text CantidadPlasmaRecolectado;
    public TMPro.TMP_Text LoLograste;
    public TMPro.TMP_Text InicioDelJuego;
    private int contGR;
    private int contGB;
    private int contPlasma;
    private int contTotal;
    public float FuerzaDeSalto = 1.0f;
    private bool SobreElSuelo = true;
    public int MaximoSaltos = 2;
    public int SaltoActual = 0;
    float xInicial, yInicial;




    void Start()
    {
        xInicial = transform.position.x;
        yInicial = transform.position.y;
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
        contGB = 0;
        contGR = 0;
        contPlasma = 0;
        
        LoLograste.text = "";
        setearTextos();
    }

    private void FixedUpdate()
    {
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        float movimientoVertical = Input.GetAxis("Vertical");
        Vector3 vectorMovimiento = new Vector3(movimientoHorizontal, 0.0f, movimientoVertical);

        rb.AddForce(vectorMovimiento * rapidez);
    }
    void Update()
    {
       
        if (Input.GetKeyDown("escape"))
        {
            //cuando se aprete escape que aparezca el cursor
            Cursor.lockState = CursorLockMode.None;
        }
        //salto doble
        if (Input.GetKeyDown("space") && (SobreElSuelo || MaximoSaltos > SaltoActual))
        {
            rb.AddForce(new Vector3(0, FuerzaDeSalto, 0), ForceMode.Impulse);
            SobreElSuelo = false;
            SaltoActual++;
        }

        if (contTotal >= 10)
        {
            GestorDeAudio.instancia.ReproducirSonido("SonidoGanaste");
        }

    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("coleccionable"))
        {
            contGR = contGR + 1;
            setearTextos();
            other.gameObject.SetActive(false);
        }

        if (other.gameObject.CompareTag("coleccionableGB"))
        {
            contGB = contGB + 1;
            setearTextos();
            other.gameObject.SetActive(false);
        }

        if (other.gameObject.CompareTag("coleccionableP"))
        {
            contPlasma = contPlasma + 1;
            setearTextos();
            other.gameObject.SetActive(false);
        }
    }

    //para que vuelva a saltar cuando toca el piso
    private void OnCollisionEnter(Collision collision)
    {
        SobreElSuelo = true;
        SaltoActual = 0;
    }

    private void setearTextos()
    {
        CantidadGBRecolectados.text = "Globulos Blancos " + contGB.ToString() + "0/10%";
        CantidadGRRecolectados.text = "Globulos Rojos " + contGR.ToString() + "0/30%";
        CantidadPlasmaRecolectado.text = "Plasma " + contPlasma.ToString() + "0/60%";

        contTotal = contGB + contGR + contPlasma;

        if (contTotal >= 10)
        {
           // Time.timeScale = 0;
            LoLograste.text = "�Lo lograste!";
        }
        /*if (contH >= 1)
        {
            TextoRapidezYTamao.text = "Te queda poco tiempo";
        }*/
    }

}