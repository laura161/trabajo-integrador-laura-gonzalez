using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private void Start()
    {
        GestorDeAudio.instancia.ReproducirSonido("SonidoJuego");
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GestorDeAudio.instancia.ReproducirSonido("SonidoSalto");
        }

    }
}